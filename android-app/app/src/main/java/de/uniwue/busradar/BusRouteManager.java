package de.uniwue.busradar;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import de.uniwue.busradar.model.BusRoute;

/**
 * Created by Thomas on 27.06.2014.
 */
public class BusRouteManager {

    private static HashMap<String, BusRoute> routes;
    private static Context context;

    public static void setUp(Context context) {
        BusRouteManager.context = context;
    }

    public static HashSet<BusRoute> getVisibleRoutes() {
        if(routes == null) {
            routes = new HashMap<String, BusRoute>();
            loadBusRoutes();
        }

        HashSet<BusRoute> visibleRoutes = new HashSet<BusRoute>();
        for(BusRoute route : routes.values()) {
            if(route.isRouteVisible())
                visibleRoutes.add(route);
        }

        return visibleRoutes;
    }

    private static void loadBusRoutes() {
        Field[] rawFiles = R.raw.class.getFields();
        HashMap<String, Integer> routeFiles = new HashMap<String, Integer>();

        try {
            for(int i = 0; i < rawFiles.length; i++) {
                String fileName = rawFiles[i].getName();

                if(fileName.contains("route")) {
                    String lineNumber = fileName.split("_")[1];
                    routeFiles.put(lineNumber, rawFiles[i].getInt(rawFiles[i]));
                }
            }
        }
        catch(IllegalAccessException ex) {
            ex.printStackTrace();
        }

        Iterator it = routeFiles.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry) it.next();
            BusRoute routeLoaded = loadBusRouteForBusLine(entry.getKey(), entry.getValue());
            routes.put(entry.getKey(), routeLoaded);
            it.remove();
        }
    }

    private static BusRoute loadBusRouteForBusLine(String lineName, int lineRouteRes) {
        if(context == null)
            throw new NullPointerException();

        InputStream is = context.getResources().openRawResource(lineRouteRes);
        Scanner scanner = new Scanner(is);
        List<LatLng> routeCoords = new LinkedList<LatLng>();

        // skip first two lines in route_xxx_x.csv
        scanner.nextLine();
        scanner.nextLine();

        while(scanner.hasNextLine()) {
            routeCoords.add(parseRouteCoordsFromCsv(scanner.nextLine()));
        }

        return new BusRoute(lineName, routeCoords);
    }

    private static LatLng parseRouteCoordsFromCsv(String coordsAsLine) {
        String[] coords = coordsAsLine.split(",");

        return new LatLng(
                Double.parseDouble(coords[0]),
                Double.parseDouble(coords[1])
        );
    }
}
