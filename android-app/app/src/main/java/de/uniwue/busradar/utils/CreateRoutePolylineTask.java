package de.uniwue.busradar.utils;

import android.os.AsyncTask;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import de.uniwue.busradar.BusRouteManager;
import de.uniwue.busradar.model.BusRoute;

/**
 * Created by Thomas on 28.06.2014.
 */
public class CreateRoutePolylineTask extends AsyncTask<Void, Void, List<PolylineOptions>> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected List<PolylineOptions> doInBackground(Void... params) {
        List<PolylineOptions> routesAsPolylines = new LinkedList<PolylineOptions>();
        HashSet<BusRoute> routes = BusRouteManager.getVisibleRoutes();

        for(BusRoute route : routes) {
            routesAsPolylines.add(route.getRouteAsPolyline());
        }

        return routesAsPolylines;
    }
}
