package de.uniwue.busradar.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Thomas on 10.06.2014.
 */
public class BusStop {

    private String id;
    private String name;
    private LatLng coordinates;
    private Direction direction;
    private double distance;

    public BusStop(String id, String name, LatLng coords, Direction direction) {
        this.id = id;
        this.name = name;
        this.coordinates = coords;
        this.direction = direction;
        this.distance = 0;
    }

    public LatLng getCoordinates() { return coordinates; }

    public double getLatitude() {
        return coordinates.latitude;
    }

    public double getLongitude() {
        return coordinates.longitude;
    }

    public String getName() {
        return name;
    }

    public double getDistance() {
        return distance;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getId() {
        return id;
    }

    public String getMarkerTitle() {
        String stopName = name;

        if(direction != Direction.Empty)
            stopName += " (" + direction + ")";

        return stopName;
    }

    /*
     * calculate difference between two gps points (lat / lon)
     * formula from: http://www.movable-type.co.uk/scripts/latlong.html
     */
    public void calculateDistance(LatLng currPos) {

        double earthRadius = 6371; // in km
        double lat1 = currPos.latitude;
        double lat2 = this.getLatitude();
        double lon1 = currPos.longitude;
        double lon2 = this.getLongitude();

        double phi1 = Math.toRadians(lat1);
        double phi2 = Math.toRadians(lat2);
        double deltaPhi = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);

        double a = Math.sin(deltaPhi / 2) * Math.sin(deltaPhi / 2) +
                Math.cos(phi1) * Math.cos(phi2) *
                        Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        this.distance = earthRadius * c;
    }

}
