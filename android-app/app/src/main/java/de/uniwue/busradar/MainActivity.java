package de.uniwue.busradar;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.view.GravityCompat;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import de.uniwue.busradar.view.SettingsFragment;


public class MainActivity extends Activity {

    private DrawerLayout drawerLayout;
    private ListView navListView;
    private ActionBarDrawerToggle drawerToggle;

    private CharSequence drawerTitle;
    private CharSequence title;
    private String[] menuItemTitles;

    private String[] fragments = {
            "de.uniwue.busradar.view.MapViewFragment",
            "de.uniwue.busradar.view.SearchFragment",
            "de.uniwue.busradar.view.NearbyFragment",
            "de.uniwue.busradar.view.FavoriteFragment",
            "de.uniwue.busradar.view.SettingsFragment" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = drawerTitle = getTitle();
        menuItemTitles = getResources().getStringArray(R.array.menuItems);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navListView = (ListView) findViewById(R.id.left_drawer);

        loadServerSettings();

        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        navListView.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, menuItemTitles));
        navListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                FragmentTransaction tx = getFragmentManager().beginTransaction();
                tx.replace(R.id.content_frame, Fragment.instantiate(MainActivity.this, fragments[position]));
                tx.commit();

                navListView.setItemChecked(position, true);
                setTitle(menuItemTitles[position]);
                drawerLayout.closeDrawer(navListView);
            }
        });

        FragmentTransaction tx = getFragmentManager().beginTransaction();
        tx.replace(R.id.content_frame, Fragment.instantiate(MainActivity.this, fragments[2]));
        tx.commit();

        // enable icon in ActionBar to open drawer when user touch on it
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        // setUp manager classes for BusRadar
        BusStopManager.setUp(this);
        BusRouteManager.setUp(this);
    }

    private void loadServerSettings() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        BusRadarRestClient.setServerIP(
                pref.getString(SettingsFragment.KEY_PREF_SERVER_IP, ""));
        BusRadarRestClient.setServerPort(
                pref.getString(SettingsFragment.KEY_PREF_SERVER_PORT, ""));
        BusStopManager.setIsStopsCombined(
                pref.getBoolean(SettingsFragment.KEY_PREF_VIEW_COMBINED_BUSSTOPS, false));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawerLayout.isDrawerOpen(navListView);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        getActionBar().setTitle(this.title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}