package de.uniwue.busradar.model;

/**
 * Created by Thomas on 27.06.2014.
 */
public enum RequestPath {

    STOPS ("/stops"),
    STOPS_COMBINED("/sstops"),
    BUSES ("/buses");

    private final String path;

    private RequestPath(String s) {
        path = s;
    }

    public String toString() {
        return path;
    }

}
