package de.uniwue.busradar.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpStatus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.uniwue.busradar.Connectivity;
import de.uniwue.busradar.utils.OnBackendCallback;
import de.uniwue.busradar.BusStopManager;
import de.uniwue.busradar.R;
import de.uniwue.busradar.model.BusStop;

public class NearbyFragment extends Fragment implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private final int MAX_ADDRESS = 5;

    private Context context;
    private TextView txtCurrentLocation;
    private ListView listViewStations;
    private SimpleAdapter adapterBusStopList;
    private ImageView imageConnectivityStatus;

    private boolean isGooglePlayServiceAvailable;
    private LocationClient locationClient;
    private Location currentLocation;
    private Geocoder geocoder;
    private ConnectivityManager connectivityManager;

    public static Fragment newInstance(Context context) {
        return new NearbyFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_nearby, null);

        txtCurrentLocation = (TextView) root.findViewById(R.id.current_location_txt);
        listViewStations = (ListView) root.findViewById(R.id.list_stations_nearby);
        imageConnectivityStatus = (ImageView) root.findViewById(R.id.status_connectivity);

        context = getActivity();
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NetworkInfo networkInfos = connectivityManager.getActiveNetworkInfo();

        if(networkInfos == null) {
            Toast.makeText(context, "No connectivity, work with offline data.", Toast.LENGTH_SHORT).show();
            changeConnectivityImage(Connectivity.OFFLINE);
            BusStopManager.loadOfflineBusStops();
            createBusStopListView();
        }
        else if(networkInfos.isAvailable() && networkInfos.isConnected()) {
            BusStopManager.loadOnlineBusStops(new OnBackendCallback() {
                @Override
                public void onRequestFinished(int statusCode) {
                    String msg;

                    if (statusCode == HttpStatus.SC_OK) {
                        changeConnectivityImage(Connectivity.ONLINE);
                        msg = "Received bus stops from backend.";
                    } else {
                        changeConnectivityImage(Connectivity.OFFLINE);
                        msg = "Load bus stops from local device storage.";
                        BusStopManager.loadOfflineBusStops();
                    }

                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    createBusStopListView();
                }
            });
        } else {
            Toast.makeText(context, "No network connection, work with offline data.", Toast.LENGTH_SHORT).show();
            changeConnectivityImage(Connectivity.OFFLINE);
            BusStopManager.loadOfflineBusStops();
            createBusStopListView();
        }

        checkGooglePlayServices();
        requestCurrentLocation();
    }

    private void changeConnectivityImage(Connectivity state) {
        Drawable image;

        switch (state) {
            case ONLINE:
                image = getResources().getDrawable(R.drawable.wifi_active);
                break;
            case OFFLINE:
                image = getResources().getDrawable(R.drawable.wifi_inactive);
                break;
            default:
                image = getResources().getDrawable(R.drawable.wifi_inactive);
        }

        imageConnectivityStatus.setImageDrawable(image);
    }

    private void createBusStopListView() {
        List<BusStop> busStopsNearby = new ArrayList<BusStop>();

        try {
            busStopsNearby = BusStopManager.findNearbyBusStops(
                    new LatLng(
                            currentLocation.getLatitude(),
                            currentLocation.getLongitude())
            );
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        List<Map<String, String>> busStopsWithDistance = new ArrayList<Map<String, String>>();

        for(BusStop bs : busStopsNearby) {
            Map<String, String> busStopDatum = new HashMap<String, String>(2);
            busStopDatum.put("First Line", bs.getName() + " (" + bs.getDirection() + ")");
            busStopDatum.put("Second Line", "Entfernung: " + Math.round(100.0 * bs.getDistance()) / 100.0 + " km");
            busStopsWithDistance.add(busStopDatum);
        }

        adapterBusStopList = new SimpleAdapter(getActivity(), busStopsWithDistance,
                android.R.layout.simple_list_item_2,
                new String[] {"First Line", "Second Line"},
                new int[] {android.R.id.text1, android.R.id.text2});
        listViewStations.setAdapter(adapterBusStopList);

        registerForContextMenu(listViewStations);

        listViewStations.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });
    }

    private void checkGooglePlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if(ConnectionResult.SUCCESS == resultCode) {
            isGooglePlayServiceAvailable = true;
            Log.d(getTag(), "GooglePlayService is available");
        }
        else {
            isGooglePlayServiceAvailable = false;
            Log.d(getTag(), "GooglePlayService is not available");
        }
    }

    private void requestCurrentLocation() {
        if(isGooglePlayServiceAvailable) {
            locationClient = new LocationClient(context, this, this);
        geocoder = new Geocoder(context);
        locationClient.connect();
    }
        else {
            txtCurrentLocation.setText(R.string.error_googleplay_services);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnected(Bundle bundle) {
        currentLocation = locationClient.getLastLocation();
        loadCurrentAddressInformation();
    }

    private void loadCurrentAddressInformation() {
        if (geocoder.isPresent()) {
            try {
                List<Address> locatedAddress = geocoder.getFromLocation(
                        currentLocation.getLatitude(),
                        currentLocation.getLongitude(),
                        MAX_ADDRESS
                );

                if (!locatedAddress.isEmpty()) {
                    Address firstResult = locatedAddress.get(0);
                    txtCurrentLocation.setText(firstResult.getAddressLine(0) + ", " +
                            firstResult.getLocality());
                } else {
                    txtCurrentLocation.setText(R.string.error_current_location);
                }
            } catch (IOException e) {
                Log.e(getTag(), e.getMessage());
                txtCurrentLocation.setText(R.string.error_current_location);
            }
        }
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(context, "Disconnected, please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(context, "Connection failed, sorry! Try again :)",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.bus_stop_item_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        HashMap<String, String> selectedItem = (HashMap) adapterBusStopList.getItem(info.position);
        String stopName = selectedItem.get("First Line");

        switch(item.getItemId()) {
            case R.id.hide_station_item:
                Toast.makeText(context, "Hide stop '" + stopName + "'", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.add_favorite_station_item:
                Toast.makeText(context, "Add stop '" + stopName + "' to favorites.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.save_station_item_home:
                Toast.makeText(context, "Saved as bus stop at home.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.save_station_item_work:
                Toast.makeText(context, "Saved as bus stop at work.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
