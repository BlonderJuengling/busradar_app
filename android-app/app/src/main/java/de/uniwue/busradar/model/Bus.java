package de.uniwue.busradar.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Thomas on 21.06.2014.
 */
public class Bus {

    private int id;
    private int line;
    private LatLng currentPosition;
    private BusStop nextStop;
    private int timeToNextStop;

    public Bus(int id, int line, LatLng position, BusStop next, int timeToNext) {
        this.id = id;
        this.line = line;
        this.currentPosition = position;
        this.nextStop = next;
        this.timeToNextStop = timeToNext;
    }

    public int getId() { return id; }

    public int getLine() {
        return line;
    }

    public LatLng getCurrentPosition() {
        return currentPosition;
    }

    public String getSnippetText() {
        if(timeToNextStop == 0) {
            if(nextStop == null)
                return "Waiting at bus stop";

            return "Waiting at " + nextStop.getName();
        }
        else {
            String text = "Next stop: ";

            if(nextStop == null)
                text += " n/A";
            else
                text += " " + nextStop.getName();

            return text + " | Arrival in: " + timeToNextStop + " sec";
        }

    }
}
