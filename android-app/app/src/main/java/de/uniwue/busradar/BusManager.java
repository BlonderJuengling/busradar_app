package de.uniwue.busradar;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.uniwue.busradar.model.Bus;
import de.uniwue.busradar.model.RequestPath;
import de.uniwue.busradar.utils.OnBackendCallback;

/**
 * Created by Thomas on 21.06.2014.
 */
public class BusManager {

    private static List<Bus> availableBuses;
    private static OnBackendCallback listener;

    public static List<Bus> findAllAvailableBuses() {
        return availableBuses;
    }

    public static List<Bus> findAllAvailableBusesByLine(int line) {
        List<Bus> busesByLine = new ArrayList<Bus>();
        for(Bus b : availableBuses) {
            if(b.getLine() == line) {
                busesByLine.add(b);
            }
        }
        return busesByLine;
    }

    private static Bus getBusById(int id) {
        for(int i = 0; i < availableBuses.size(); i++)
            return availableBuses.get(i);

        return null;
    }

    public static void loadAvailableBuses(OnBackendCallback callback) {
        listener = callback;
        availableBuses = new ArrayList<Bus>();

        BusRadarRestClient.get(RequestPath.BUSES.toString(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                for(int i = 0; i < response.length(); i++) {
                    try {
                        availableBuses.add(parseBusFromJson(response.getJSONObject(i)));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
                listener.onRequestFinished(statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                listener.onRequestFinished(statusCode);
            }
        });
    }

    public static Bus parseBusFromJson(JSONObject bus) throws JSONException {
        String[] coordsAsString = bus.getString("Coord").split(",");
        String nextStopID = bus.getJSONObject("NextStop").getString("ID");

        return new Bus(
                bus.getInt("ID"),
                bus.getInt("Line"),
                new LatLng(Double.parseDouble(coordsAsString[0]), Double.parseDouble(coordsAsString[1])),
                BusStopManager.getBusStopByID(nextStopID),
                Integer.parseInt(bus.getString("NextETA"))
        );
    }
}
