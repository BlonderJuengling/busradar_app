package de.uniwue.busradar.model;

/**
 * Created by Thomas on 21.06.2014.
 */
public enum Direction {
    UnknownDirection,
    Empty,
    N,
    NW,
    W,
    SW,
    S,
    SE,
    E,
    NE;

    public static Direction parseDirection(String direction) {
        if(direction.equals("N"))
            return N;
        else if(direction.equals("NW"))
            return NW;
        else if(direction.equals("W"))
            return W;
        else if(direction.equals("SW"))
            return SW;
        else if(direction.equals("S"))
            return S;
        else if(direction.equals("SE"))
            return SE;
        else if(direction.equals("E"))
            return E;
        else if(direction.equals("NE"))
            return NE;
        else if(direction.equals("_"))
            return Empty;
        else
            return UnknownDirection;
    }
}


