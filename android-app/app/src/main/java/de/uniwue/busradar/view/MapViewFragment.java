package de.uniwue.busradar.view;

import android.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.uniwue.busradar.BusManager;
import de.uniwue.busradar.BusStopManager;
import de.uniwue.busradar.utils.CreateRoutePolylineTask;
import de.uniwue.busradar.utils.OnBackendCallback;
import de.uniwue.busradar.R;
import de.uniwue.busradar.model.Bus;
import de.uniwue.busradar.model.BusStop;

/**
 * Created by Thomas on 09.06.2014.
 */
public class MapViewFragment extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks,
    GooglePlayServicesClient.OnConnectionFailedListener {

    private Context context;
    private ViewGroup root;

    private MapFragment mapFragment;
    private GoogleMap map;
    private LocationClient locationClient;
    private ConnectivityManager connectivityManager;

    private Timer busRequestTimer;
    private Map<Integer, Marker> busMarkers;
    private List<Bus> buses;

    public static Fragment newInstance(Context context) {
        return new MapViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_map, null);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_view);

        context = getActivity();
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        map = mapFragment.getMap();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Handler handler = new Handler();
        busRequestTimer = new Timer();

        // start async http request to backend in it's own thread
        TimerTask updateBusTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        BusManager.loadAvailableBuses(new OnBackendCallback() {
                            @Override
                            public void onRequestFinished(int statusCode) {
                                buses = BusManager.findAllAvailableBuses();
                                updateBusMarkers();
                            }
                        });
                    }
                });
            }
        };
        busRequestTimer.schedule(updateBusTask, 2000, 1000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        removeMapFragment(); // prototyp workaround caused by mapFragment
    }

    @Override
    public void onPause() {
        super.onPause();

        busRequestTimer.cancel();
        removeMapFragment(); // prototyp workaround caused by mapFragment
    }

    private void removeMapFragment() {
        MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.map_view);

        if(f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        locationClient = new LocationClient(context, this, this);
        locationClient.connect();

        if (map!=null) {
            initMapView();
            addBusStopMarkers();
            addBusRoutes();

            BusManager.loadAvailableBuses(new OnBackendCallback() {
                @Override
                public void onRequestFinished(int statusCode) {
                    if (statusCode == HttpStatus.SC_OK) {
                        buses = BusManager.findAllAvailableBuses();
                        addBusMarkers();
                    } else {
                        Toast.makeText(context, "Offline mode - no live data available", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        MapsInitializer.initialize(this.getActivity());
    }

    private void initMapView() {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(false);
    }

    private void addBusMarkers() {
        buses = BusManager.findAllAvailableBuses();
        busMarkers = new HashMap<Integer, Marker>();

        for(Bus bus : buses) {
            LatLng coordinates = new LatLng(bus.getCurrentPosition().latitude, bus.getCurrentPosition().longitude);
            busMarkers.put(
                    bus.getId(),
                    map.addMarker(
                            new MarkerOptions()
                                    .position(coordinates).title("Bus-ID: " + bus.getId())
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_icon_marker))
                                    .draggable(false)
                                    .snippet(bus.getSnippetText())
                    )
            );
        }
    }

    private void addBusRoutes() {
        if(map != null) {
            List<PolylineOptions> routes;

            try {
                routes = new CreateRoutePolylineTask().execute().get();
            } catch (Exception ex) {
                ex.printStackTrace();
                routes = new LinkedList<PolylineOptions>();
            }

            for (PolylineOptions route : routes)
                map.addPolyline(route);
        }
    }

    private void updateBusMarkers() {
        if(busMarkers != null && buses != null) {
            for(Bus bus : buses) {
                Marker marker = busMarkers.get(bus.getId());

                marker.setPosition(bus.getCurrentPosition());
                marker.setSnippet(bus.getSnippetText());
            }
        }
    }

    private void addBusStopMarkers() {
        try {
            List<BusStop> busStopList = BusStopManager.getAllBusStops();
            for(BusStop busStop : busStopList) {
                LatLng coordinates = new LatLng(busStop.getLatitude(), busStop.getLongitude());
                map.addMarker(new MarkerOptions().position(coordinates)
                        .title(busStop.getMarkerTitle())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop_icon_marker))
                        .anchor(0.5f, 0.5f).draggable(false));
            }
        } catch (Exception ex) {
            Toast.makeText(context, "Error loading bus stops", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location currLocation = locationClient.getLastLocation();
        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(context, "Connection lost, please re-connect", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(context, "Connection failed, sorry", Toast.LENGTH_SHORT).show();
    }
}
