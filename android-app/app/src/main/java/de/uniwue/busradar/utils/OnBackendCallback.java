package de.uniwue.busradar.utils;

/**
 * Created by Thomas on 24.06.2014.
 */
public interface OnBackendCallback {

    void onRequestFinished(int statusCode);

}
