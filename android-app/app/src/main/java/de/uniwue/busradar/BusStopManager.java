package de.uniwue.busradar;

import android.content.Context;
import android.preference.PreferenceFragment;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import de.uniwue.busradar.model.BusStop;
import de.uniwue.busradar.model.Direction;
import de.uniwue.busradar.model.RequestPath;
import de.uniwue.busradar.utils.OnBackendCallback;

/**
 * Created by Thomas on 10.06.2014.
 */
public class BusStopManager extends PreferenceFragment {

    private static InputStream inputStream;
    private static Context context;
    private static List<BusStop> busStops;
    private static OnBackendCallback listener;
    private static boolean isStopsCombined = false;

    public static void setUp(Context context) {
        BusStopManager.context = context;
    }

    public static void setIsStopsCombined(boolean value) {
        isStopsCombined = value;
    }

    public static List<BusStop> getAllBusStops() throws Exception {
        return busStops;
    }

    public static List<BusStop> findNearbyBusStops(LatLng currentPosition) throws Exception {
        for(BusStop stop : busStops) {
            stop.calculateDistance(currentPosition);
        }

        Collections.sort(busStops, new BusStopDistanceComparator());
        return busStops;
    }

    public static BusStop getBusStopByID(String id) {
        for(BusStop stop : busStops) {
            if(stop.getId().equals(id)) {
                return stop;
            }
        }

        return null;
    }

    public static void loadOnlineBusStops(OnBackendCallback callback) {
        listener = callback;
        busStops = new ArrayList<BusStop>();

        BusRadarRestClient.get(buildRequestPath(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        busStops.add(parseBusStopFromJson(response.getJSONObject(i)));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
                listener.onRequestFinished(statusCode);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                listener.onRequestFinished(statusCode);
            }
        });
    }

    private static String buildRequestPath() {
        if(isStopsCombined)
            return RequestPath.STOPS_COMBINED.toString();

        return RequestPath.STOPS.toString();
    }

    private static BusStop parseBusStopFromJson(JSONObject busStop) throws JSONException {
        String tempId = busStop.getString("ID");

        if(isStopsCombined) // dirty hack to fake id to busStop object
            tempId = tempId.concat(" _");

        String[] id = tempId.split(" ");
        String direction = id[id.length -1];
        String[] coords = busStop.getString("Coord").split(",");

        return new BusStop(
                busStop.getString("ID"),
                busStop.getString("Name"),
                new LatLng(Double.parseDouble(coords[0]),
                        Double.parseDouble(coords[1])),
                Direction.parseDirection(direction)
        );
    }

    public static void loadOfflineBusStops() {
        InputStream is = context.getResources().openRawResource(R.raw.stops);
        Scanner scan = new Scanner(is);
        busStops = new ArrayList<BusStop>();

        while (scan.hasNextLine()) {
            busStops.add(parseBusStopFromCsv(scan.nextLine()));
        }
    }

    private static BusStop parseBusStopFromCsv(String busStop) {
        String[] stop = busStop.split(",");

        LatLng coords = new LatLng(Double.parseDouble(stop[1]), Double.parseDouble(stop[0]));
        String id = stop[2];
        String[] tmp = id.split(" ");
        Direction direction = Direction.parseDirection(tmp[tmp.length - 1]);
        String name = id.substring(0, id.lastIndexOf(" "));

        return new BusStop(id, name, coords, direction);
    }
}
