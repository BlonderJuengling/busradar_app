package de.uniwue.busradar;

/**
 * Created by Thomas on 26.06.2014.
 */
public enum Connectivity {
    ONLINE,
    OFFLINE
}
