package de.uniwue.busradar.model;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by Thomas on 27.06.2014.
 */
public class BusRoute {

    private final int ROUTE_LINE_WIDTH = 8;

    private String lineName;
    private boolean isVisible;
    private int lineColor;
    private List<LatLng> routeCoords;

    public BusRoute(String line, List<LatLng> coords) {
        this.lineName = line;
        this.isVisible = true;
        this.routeCoords = coords;
        this.lineColor = getRandomColor();
    }

    private int getRandomColor() {
        Random r = new Random();
        return Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255));
    }

    public boolean isRouteVisible() {
        return isVisible;
    }

    public PolylineOptions getRouteAsPolyline() {
        return new PolylineOptions()
                .addAll(routeCoords)
                .width(ROUTE_LINE_WIDTH)
                .color(lineColor);
    }
}
