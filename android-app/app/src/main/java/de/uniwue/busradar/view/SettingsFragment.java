package de.uniwue.busradar.view;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import de.uniwue.busradar.BusRadarRestClient;
import de.uniwue.busradar.BusStopManager;
import de.uniwue.busradar.utils.OnBackendCallback;
import de.uniwue.busradar.R;

/**
 * Created by Thomas on 09.06.2014.
 */
public class SettingsFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_PREF_SERVER_IP = "pref_server_ip";
    public static final String KEY_PREF_SERVER_PORT = "pref_server_port";
    public static final String KEY_PREF_VIEW_COMBINED_BUSSTOPS = "pref_view_combined_busstops";

    public static Fragment newInstance(Context context) {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        loadSettingsToSummary();
    }

    private void loadSettingsToSummary() {
        setPreferenceSummary(KEY_PREF_SERVER_IP);
        setPreferenceSummary(KEY_PREF_SERVER_PORT);
    }

    private void setPreferenceSummary(String preferenceKey) {
        SharedPreferences preferences = getPreferenceScreen().getSharedPreferences();

        Preference pref = findPreference(preferenceKey);
        pref.setSummary(preferences.getString(preferenceKey, ""));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(KEY_PREF_SERVER_IP)) {
            String value = getPreferenceScreen().getSharedPreferences().getString(key, "");
            setPreferenceSummary(key);
            BusRadarRestClient.setServerIP(value);
        }
        if(key.equals(KEY_PREF_SERVER_PORT)) {
            String value = getPreferenceScreen().getSharedPreferences().getString(key, "");
            setPreferenceSummary(key);
            BusRadarRestClient.setServerPort(value);
        }
        if(key.equals(KEY_PREF_VIEW_COMBINED_BUSSTOPS)) {
            final boolean value = getPreferenceScreen().getSharedPreferences().getBoolean(key, false);
            BusStopManager.setIsStopsCombined(value);
            BusStopManager.loadOnlineBusStops(new OnBackendCallback() {
                @Override
                public void onRequestFinished(int statusCode) {
                    System.out.println("Refresh BusStops after change settings. BusStops combined: " + value);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}
