package de.uniwue.busradar;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by Thomas on 24.06.2014.
 */
public class BusRadarRestClient {

    private static String SERVER_IP;
    private static String SERVER_PORT;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void setServerIP(String ip) {
        BusRadarRestClient.SERVER_IP = ip;
    }

    public static void setServerPort(String port) {
        BusRadarRestClient.SERVER_PORT = port;
    }

    public static void get(String url, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), responseHandler);
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return "http://" + SERVER_IP + ":" + SERVER_PORT + relativeUrl;
    }
}
