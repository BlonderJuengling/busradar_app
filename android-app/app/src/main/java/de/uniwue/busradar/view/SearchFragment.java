package de.uniwue.busradar.view;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.uniwue.busradar.R;

/**
 * Created by Thomas on 09.06.2014.
 */
public class SearchFragment extends Fragment {

    public static Fragment newInstance(Context context) {
        return new SearchFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_search, null);
        return root;
    }
}
