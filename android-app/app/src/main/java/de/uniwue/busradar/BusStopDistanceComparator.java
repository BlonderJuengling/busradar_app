package de.uniwue.busradar;

import java.util.Comparator;

import de.uniwue.busradar.model.BusStop;

/**
 * Created by Thomas on 10.06.2014.
 */
public class BusStopDistanceComparator implements Comparator<BusStop> {

    @Override
    public int compare(BusStop lhs, BusStop rhs) {
        if(lhs.getDistance() < rhs.getDistance())
            return -1;

        if(lhs.getDistance() > rhs.getDistance())
            return 1;

        return 0;
    }
}
